# Gjøre


## Description
iOS app for discovering a lot of things in one place, like movies, books and else.
Soon it will contains wish and done lists, and other interesting features.


## Technology stack

##### Core
* Swift
* Foundation
* Grand Central Dispatch (GCD)
* Protocol Oriented
* MVVM based architecture
* Routers for navigation
* Unit tests (XCTest)

##### API
* URLSession
* Codable classes
* Services for REST API endpoints
* Loading images in background and caching

##### UI
* UIKit
* Core Graphics
* Core Animation
* Interface Builder and Auto Layouts
* TabBar based navigation
* Custom transitions
