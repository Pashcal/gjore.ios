//
//  CardViewController.swift
//  Gjore
//
//  Created by Pavel Chupryna on 10/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import UIKit

class CardViewController: UIViewController, ViewControllerProtocol {
    
    private var viewModel: CardViewModel!
    
    var interactionController: UIPercentDrivenInteractiveTransition?
    
    @IBOutlet private weak var headerImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    @IBAction func closeAction(_ sender: Any) {
        viewModel.close()
    }
    
    func initViewModel(_ viewModel: CardViewModel) {
        self.viewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let statusBarBackgroundView = UIVisualEffectView(frame: UIApplication.shared.statusBarFrame)
        statusBarBackgroundView.effect = UIBlurEffect(style: .prominent)
        view.addSubview(statusBarBackgroundView)
        
        titleLabel.text = viewModel.title
        headerImageView.loadImage(viewModel.headerImage?.medium)
        headerImageView.loadImage(viewModel.headerImage?.hight)
        descriptionLabel.text = nil
        
        activityIndicator.show()
        viewModel.detailsChanged = onDetailsChanged
        viewModel.onViewDidLoad()
        
        prepareForInteractionController()
    }
    
    func onDetailsChanged() {
        activityIndicator.hide()
        descriptionLabel.text = viewModel.description
    }
}
