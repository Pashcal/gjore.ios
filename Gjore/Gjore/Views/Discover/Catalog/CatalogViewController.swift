//
//  CatalogViewController.swift
//  Gjore
//
//  Created by Pavel Chupryna on 10/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import UIKit

class CatalogViewController: UIViewController, ViewControllerProtocol {
    
    private var viewModel: CatalogViewModel!
    
    private let collectionViewSource = CatalogCollectionViewSource()
    
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var emptyView: UIView!
    
    func initViewModel(_ viewModel: CatalogViewModel) {
        self.viewModel = viewModel
    }
    
    override func viewDidLoad() {
        
        collectionView.register(CatalogCollectionViewCell.nib, forCellWithReuseIdentifier: CatalogCollectionViewCell.key)
        let collectionViewLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        collectionViewLayout.itemSize = CGSize(width: UIScreen.main.bounds.width - collectionViewLayout.sectionInset.left - collectionViewLayout.sectionInset.right, height: 160)
        
        collectionViewSource.itemSelected = onItemSelected(item:)
        collectionView.dataSource = collectionViewSource
        collectionView.delegate = collectionViewSource
        collectionView.reloadData()
        
        let statusBarBackgroundView = UIVisualEffectView(frame: UIApplication.shared.statusBarFrame)
        statusBarBackgroundView.effect = UIBlurEffect(style: .prominent)
        view.addSubview(statusBarBackgroundView)
        
        activityIndicator.show()
        viewModel.itemsChanged = onItemsChanged
        viewModel.onViewDidLoad()
    }
    
    func onItemsChanged(items: [CatalogItemViewModel]?, from: Int) {
        self.activityIndicator.hide()
        self.emptyView.hide()
        guard let items = items else {
            self.emptyView.show()
            return
        }
        
        self.collectionViewSource.items = items
        var insertItems:[IndexPath] = []
        for i in 0..<items.count {
            insertItems.append(IndexPath(row: i, section: 0))
        }
        UIView.animate(withDuration: 0.5) {
            self.collectionView.insertItems(at: insertItems)
        }
    }
    
    func onItemSelected(item: CatalogItemViewModel) {
        viewModel.onItemSelected(item)
    }
}
