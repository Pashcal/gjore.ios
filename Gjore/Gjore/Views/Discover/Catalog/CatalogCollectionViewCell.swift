//
//  CatalogCollectionViewCell.swift
//  Gjore
//
//  Created by Pavel Chupryna on 10/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import UIKit

class CatalogCollectionViewCell: UICollectionViewCell, UIGestureRecognizerDelegate {

    static let key = "CatalogCell"
    static let nib = UINib(nibName: "CatalogCollectionViewCell", bundle: nil)
    
    private let cornerRadius: CGFloat = 12
    private let shadowRadius: CGFloat = 8
    private let shadowRadiusPressed: CGFloat = 3
    private var pressAnimationButton: UIButton?
    private let selectionFeedbackGenerator = UISelectionFeedbackGenerator()
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var backgroundImageView: UIImageView!
    
    var selectAction: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()

        let button = UIButton()
        button.layer.cornerRadius = cornerRadius
        button.addTarget(self, action: #selector(pressAnimationAction(_:)), for: .touchDown)
        button.addTarget(self, action: #selector(pressAnimationCancel(_:)), for: .touchCancel)
        button.addTarget(self, action: #selector(pressAnimationCancel(_:)), for: .touchUpOutside)
        button.addTarget(self, action: #selector(selectAction(_:)), for: .touchUpInside)

        contentView.addSubview(button)
        button.expandToSuperviewBounds()
        pressAnimationButton = button

//        let gradiendLayer = CAGradientLayer()
//        gradiendLayer.frame = bounds
//        gradiendLayer.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
//        contentView.layer.addSublayer(gradiendLayer)
//        contentView.bringSubview(toFront: titleLabel)

        contentView.layer.cornerRadius = cornerRadius
        contentView.layer.masksToBounds = true
        
        layer.cornerRadius = cornerRadius
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.3
        layer.shadowOffset = CGSize(width: 0, height: 1)
        layer.shadowRadius = shadowRadius
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        clipsToBounds = false
        setNeedsLayout()
    }

    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        if layer.shadowPath == nil {
            layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: layer.cornerRadius).cgPath
        }
    }
    
    func setup(_ item: CatalogItemViewModel) {
        titleLabel.text = item.title
        backgroundImageView.loadImage(item.backgroundImage?.low)
        backgroundImageView.loadImage(item.backgroundImage?.medium)
    }

    private let pressDuration = 0.1
    private let pressScale: CGFloat = 0.95

    @IBAction func pressAnimationAction(_ sender: Any) {
        let layerAnim = CABasicAnimation(keyPath: #keyPath(CALayer.shadowRadius))
        layerAnim.fromValue = shadowRadius
        layerAnim.toValue = shadowRadiusPressed
        layerAnim.duration = pressDuration
        layer.shadowRadius = shadowRadiusPressed
        layer.add(layerAnim, forKey: layerAnim.keyPath)
        UIView.animate(withDuration: pressDuration) {
            self.transform = CGAffineTransform(scaleX: self.pressScale, y: self.pressScale)
        }
    }

    @IBAction func pressAnimationCancel(_ sender: Any) {
        let layerAnim = CABasicAnimation(keyPath: #keyPath(CALayer.shadowRadius))
        layerAnim.fromValue = shadowRadiusPressed
        layerAnim.toValue = shadowRadius
        layerAnim.duration = pressDuration
        layer.shadowRadius = shadowRadius
        layer.add(layerAnim, forKey: layerAnim.keyPath)
        UIView.animate(withDuration: pressDuration) {
            self.transform = CGAffineTransform.identity
        }
    }

    @IBAction func selectAction(_ sender: Any) {
        selectAction?()
        pressAnimationCancel(sender)
    }
}
