//
//  CatalogCollectionViewSource.swift
//  Gjore
//
//  Created by Pavel Chupryna on 10/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation
import UIKit

class CatalogCollectionViewSource: NSObject, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var items: [CatalogItemViewModel]?
    var itemSelected: ((CatalogItemViewModel) -> Void)?
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CatalogCollectionViewCell.key, for: indexPath) as! CatalogCollectionViewCell
        
        let item = items![indexPath.row]
        
        cell.setup(item)
        cell.selectAction = { self.collectionView(collectionView, didSelectItemAt: indexPath) }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let itemSelected = itemSelected, let items = items, indexPath.row < items.count else {
            return
        }
        
        itemSelected(items[indexPath.row])
    }
}
