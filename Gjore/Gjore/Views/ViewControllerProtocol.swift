//
//  BaseViewController.swift
//  Gjore
//
//  Created by Pavel Chupryna on 24/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation
import UIKit

protocol ViewControllerProtocol: class {
    
    associatedtype ViewModelType: ViewModelProtocol
    
    func initViewModel(_ viewModel: ViewModelType)
}
