//
//  MainViewController.swift
//  Gjore
//
//  Created by Pavel Chupryna on 10/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewControllers = [
            Tabs.catalog.tabBarViewController,
            Tabs.search.tabBarViewController,
            Tabs.profile.tabBarViewController
        ]
    }
}

private enum Tabs: String, CaseIterable {
    case catalog = "Catalog"
    case search = "Search"
    case profile = "Profile"
    
    private var icon: UIImage? {
        var name = ""
        switch self {
        case .catalog:
            name = "photo.on.rectangle"
        case .search:
            name = "magnifyingglass"
        case .profile:
            name = "person"
        }
        
        if #available(iOS 13.0, *) {
            return UIImage(systemName: name)
        }
        
        return UIImage(named: name)
    }
    
    private func navigationController(for rootVC: UIViewController) -> UINavigationController {
        let nc = UINavigationController(rootViewController: rootVC)
        nc.navigationBar.prefersLargeTitles = true
        rootVC.navigationItem.title = self.title
        rootVC.tabBarItem = self.tabBarItem
        return nc
    }
    
    var title: String {
        return self.rawValue
    }
    
    var tabBarItem: UITabBarItem {
        return UITabBarItem(title: self.title, image: self.icon, tag: 0)
    }
    
    // TODO: Think about improvements with generics
    var tabBarViewController: UIViewController {
        switch self {
        case .catalog:
            let vc = CatalogViewController(nibName: String(describing: CatalogViewController.self), bundle: nil)
            let nc = navigationController(for: vc)
            
            let router = DiscoverRouter(nc)
            let viewModel = CatalogViewModel(router)
            vc.initViewModel(viewModel)
            
            return nc
            
        case .search:
            let vc = SearchViewController(nibName: String(describing: SearchViewController.self), bundle: nil)
            let nc = navigationController(for: vc)
            return nc
            
        case .profile:
            let vc = ProfileViewController(nibName: String(describing: ProfileViewController.self), bundle: nil)
            let nc = navigationController(for: vc)
            return nc
        }
    }
}
