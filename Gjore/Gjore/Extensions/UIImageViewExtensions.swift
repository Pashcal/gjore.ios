//
//  UIImageViewExtensions.swift
//  Gjore
//
//  Created by Pavel Chupryna on 30/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    
    static let cache = NSCache<NSString, UIImage>()
    
    func loadImage(_ url: String?) {
        self.image = nil
        
        guard let url = url else {
            return
        }
        
        let nsUrl = NSString(string: url)
        if let imageCache = UIImageView.cache.object(forKey: nsUrl) {
            self.image = imageCache
        }
        else {
            DispatchQueue.global().async {
                do {
                    let imageData = try Data(contentsOf: URL(string: url)!)
                    let image = UIImage(data: imageData)!
                    UIImageView.cache.setObject(image, forKey: nsUrl)
                    DispatchQueue.main.async {
                        self.image = image
                    }
                }
                catch {
                    
                }
            }
        }
    }
}
