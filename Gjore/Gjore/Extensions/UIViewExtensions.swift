//
//  UIViewExtensions.swift
//  Gjore
//
//  Created by Pavel Chupryna on 24/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import UIKit

extension UIView {
    
    func show() {
        if !self.isHidden {
            return
        }
                
        self.alpha = 0
        self.isHidden = false
        UIView.animate(withDuration: 0.23) {
            self.alpha = 1
        }
    }
    
    func hide() {
        if self.isHidden {
            return
        }
        
        self.alpha = 1
        UIView.animate(withDuration: 0.23, animations: {
            self.alpha = 0
        }) { _ in
            self.isHidden = true
        }
    }

    func expandToSuperviewBounds() {
        guard let superview = superview else {
            return
        }
        self.translatesAutoresizingMaskIntoConstraints = false
        let leftConstraint = self.leftAnchor.constraint(equalTo: superview.leftAnchor)
        let topConstraint = self.topAnchor.constraint(equalTo: superview.topAnchor)
        let rightConstraint = self.rightAnchor.constraint(equalTo: superview.rightAnchor)
        let bottomConstraint = self.bottomAnchor.constraint(equalTo: superview.bottomAnchor)
        NSLayoutConstraint.activate([leftConstraint, topConstraint, rightConstraint, bottomConstraint])
    }
}
