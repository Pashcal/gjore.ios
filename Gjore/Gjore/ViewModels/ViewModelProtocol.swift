//
//  BaseViewModel.swift
//  Gjore
//
//  Created by Pavel Chupryna on 24/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation

protocol ViewModelProtocol: class {
    
//    associatedtype RouterType: RouterProtocol
    
//    init(_ router: RouterType)
    
    func onViewDidLoad()
}
