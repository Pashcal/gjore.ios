//
//  CardViewModel.swift
//  Gjore
//
//  Created by Pavel Chupryna on 26/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation

class CardViewModel: ViewModelProtocol {
    
    private let router: DiscoverRouter!
    private let detailsService: DetailsServiceProtocol!
    
    let id: Int!
    var headerImage: Image!
    let title: String!
    var description: String!
    
    var detailsChanged: (() -> Void)?
    
    required init(_ router: DiscoverRouter?, catalogItem: CatalogItemViewModel!, detailsService: DetailsServiceProtocol? = nil) {
        self.router = router
        id = catalogItem.id
        headerImage = catalogItem.backgroundImage
        title = catalogItem.title
        
        self.detailsService = detailsService ?? MoviesService()
    }
    
    func onViewDidLoad() {
        detailsService.getDetails(for: id) { data in
            guard let data = data else {
                self.description = ""
                return
            }
            
            self.description = data.description
            self.detailsChanged?()
        }
    }
    
    func close() {
        router.dismiss()
    }
}
