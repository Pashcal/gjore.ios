//
//  CatalogItemViewModel.swift
//  Gjore
//
//  Created by Pavel Chupryna on 24/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation

class CatalogItemViewModel: Equatable {
    
    let id: Int!
    let backgroundImage: Image?
    let title: String!
    
    init(_ catalogItem: CatalogItemModelProtocol) {
        id = catalogItem.id
        backgroundImage = catalogItem.backgroundImage
        title = catalogItem.title
    }
    
    static func == (lhs: CatalogItemViewModel, rhs: CatalogItemViewModel) -> Bool {
        return (lhs.id == rhs.id && lhs.backgroundImage == rhs.backgroundImage && lhs.title == rhs.title)
    }
}
