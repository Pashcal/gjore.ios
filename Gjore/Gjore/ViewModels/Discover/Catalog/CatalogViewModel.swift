//
//  CatalogViewModel.swift
//  Gjore
//
//  Created by Pavel Chupryna on 24/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation

class CatalogViewModel: ViewModelProtocol {
    
    private let router: DiscoverRouter!
    private let discoverService: DiscoverServiceProtocol!
    
    var items: [CatalogItemViewModel]?
    var itemsChanged: (([CatalogItemViewModel]?, Int) -> Void)?
    
    required init(_ router: DiscoverRouter?, discoverService: DiscoverServiceProtocol? = nil) {
        self.router = router
        self.discoverService = discoverService ?? DiscoverService()
    }
    
    func onViewDidLoad() {
        discoverService.getCatalogItems { data in
            guard let data = data else {
                self.items = nil
                return
            }
            
            self.items = data.map { movie in
                return CatalogItemViewModel(movie)
            }
            self.itemsChanged?(self.items, 0)
        }
    }
    
    func onItemSelected(_ item: CatalogItemViewModel) {
        router.route(to: .card(catalogItem: item))
    }
}
