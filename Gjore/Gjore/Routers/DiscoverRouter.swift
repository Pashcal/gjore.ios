//
//  DiscoverRouter.swift
//  Gjore
//
//  Created by Pavel Chupryna on 28/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation
import UIKit

class DiscoverRouter: RouterProtocol {
    
    private weak var navigationController: UINavigationController!
    
    enum Destination {
        case card(catalogItem: CatalogItemViewModel)
    }
    
    required init(_ navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func route(to destination: Destination) {
        guard let navigationController = navigationController else {
            return
        }
        
        navigationController.present(makeViewController(for: destination), animated: true, completion: nil)
    }
    
    private func makeViewController(for destination: Destination) -> UIViewController {
        switch destination {
        case .card(let catalogItem):
            let cardRouter = self
            let cardViewModel = CardViewModel(cardRouter, catalogItem: catalogItem)
            let cardViewController = CardViewController(nibName: String(describing: CardViewController.self), bundle: nil)
            cardViewController.initViewModel(cardViewModel)
            cardViewController.transitioningDelegate = cardViewController
            cardViewController.modalPresentationStyle = .custom
            cardViewController.modalPresentationCapturesStatusBarAppearance = true
            return cardViewController
        }
    }
    
    func dismiss() {
        navigationController.dismiss(animated: true, completion: nil)
    }
}
