//
//  RouterProtocol.swift
//  Gjore
//
//  Created by Pavel Chupryna on 28/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation
import UIKit

protocol RouterProtocol {
    
    associatedtype Destination
    
    init(_ navigationController: UINavigationController)
    
    func route(to destination: Destination)
    
    func dismiss()
}
