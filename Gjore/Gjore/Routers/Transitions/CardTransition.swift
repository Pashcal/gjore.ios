//
//  CollectionCardTransition.swift
//  Gjore
//
//  Created by Pavel Chupryna on 01/06/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation
import UIKit

class CardTransitionAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    let isPresenting: Bool!
    let duration: TimeInterval!
    
    init(isPresenting: Bool, duration: TimeInterval = 0.5) {
        self.isPresenting = isPresenting
        self.duration = duration
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let fromVC = transitionContext.viewController(forKey: .from)!
        let toVC = transitionContext.viewController(forKey: .to)!
        
        if isPresenting {
            toVC.view.alpha = 0
            toVC.view.frame = UIScreen.main.bounds
            containerView.addSubview(toVC.view)
        }
        else {
            fromVC.view.alpha = 1
        }
        
        UIView.animate(withDuration: duration, animations: {
            if self.isPresenting { toVC.view.alpha = 1 }
            else { fromVC.view.alpha = 0 }
        }) { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
}

extension CardViewController: UIViewControllerTransitioningDelegate {
    
    func prepareForInteractionController() {
        let edgePanGR = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.handleInteractionEdgePan(_:)))
        edgePanGR.edges = .left
        self.view.addGestureRecognizer(edgePanGR)
    }
    
    @objc func handleInteractionEdgePan(_ gesture: UIScreenEdgePanGestureRecognizer) {
        let translate = gesture.translation(in: gesture.view)
        let percent = translate.x / gesture.view!.bounds.size.width
        
        switch gesture.state {
        case .began:
            self.interactionController = UIPercentDrivenInteractiveTransition()
            self.dismiss(animated: true, completion: nil)
        case .changed:
            self.interactionController?.update(percent)
        case .ended:
            let velocity = gesture.velocity(in: gesture.view)
            
            if percent > 0.5 || velocity.x > 0 {
                self.interactionController?.finish()
            }
            else {
                self.interactionController?.cancel()
            }
            self.interactionController = nil
        default:
            break
        }
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return CardTransitionAnimator(isPresenting: true)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return CardTransitionAnimator(isPresenting: false)
    }
    
    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return nil
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return self.interactionController
    }
}
