//
//  DetailesServiceProtocol.swift
//  Gjore
//
//  Created by Pavel Chupryna on 08/06/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation

protocol DetailsServiceProtocol {
    func getDetails(for id: Int, complete: @escaping (CardModelProtocol?) -> Void)
}
