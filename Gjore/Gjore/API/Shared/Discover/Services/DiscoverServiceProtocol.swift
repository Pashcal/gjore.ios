//
//  DiscoverServiceProtocol.swift
//  Gjore
//
//  Created by Pavel Chupryna on 08/06/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation

protocol DiscoverServiceProtocol {
    func getCatalogItems(complete: @escaping ([CatalogItemModelProtocol]?) -> Void)
}
