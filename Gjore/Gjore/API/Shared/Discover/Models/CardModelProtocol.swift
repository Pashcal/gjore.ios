//
//  CardModelProtocol.swift
//  Gjore
//
//  Created by Pavel Chupryna on 29/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation

protocol CardModelProtocol: Decodable {
    var id: Int! { get }
    var headerImage: Image? { get }
    var title: String! { get }
    var description: String! { get }
}
