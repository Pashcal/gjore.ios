//
//  ImageProtocol.swift
//  Gjore
//
//  Created by Pavel Chupryna on 29/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation

class Image: Hashable, Equatable {
    
    let low: String?
    let medium: String?
    let hight: String?
    
    init(low: String? = nil, medium: String? = nil, hight: String? = nil) {
        self.low = low
        self.medium = medium
        self.hight = hight
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.low)
        hasher.combine(self.medium)
        hasher.combine(self.hight)
    }
    
    static func == (lhs: Image, rhs: Image) -> Bool {
        return (lhs.low == rhs.low &&
            lhs.medium == rhs.medium &&
            lhs.hight == rhs.hight)
    }
}
