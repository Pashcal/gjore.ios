//
//  CatalogItemModel.swift
//  Gjore
//
//  Created by Pavel Chupryna on 24/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation

protocol CatalogItemModelProtocol: Decodable {
    var id: Int! { get }
    var backgroundImage: Image? { get }
    var title: String! { get }
}
