//
//  MoviesConfig.swift
//  Gjore
//
//  Created by Pavel Chupryna on 24/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//
import Foundation

class MoviesConfig {
    static let baseUrl = "https://api.themoviedb.org/3"
    static let apiKey = "1f823bf578802f89535a09d9bfb3276b"
    
    static let imagesBaseUrl = "https://image.tmdb.org/t/p/"
    enum BackdropSizes: String {
        case low = "w300"
        case medium = "w780"
        case hight = "w1280"
    }
}
