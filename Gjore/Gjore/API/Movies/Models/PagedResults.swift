//
//  PagedResults.swift
//  Gjore
//
//  Created by Pavel Chupryna on 10/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation

class PagedResults<T: Codable>: Codable {
    var page: Int
    var totalResults: Int
    var totalPages: Int
    var results: [T]
    
    enum CodingKeys: String, CodingKey {
        case page
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case results
    }
    
    init(page: Int, totalResults: Int, totalPages: Int, results: [T]) {
        self.page = page
        self.totalResults = totalResults
        self.totalPages = totalPages
        self.results = results
    }
}
