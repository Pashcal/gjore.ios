//
//  Movie.swift
//  Gjore
//
//  Created by Pavel Chupryna on 10/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation

class Movie: Codable {
    let voteCount: Int
    let id: Int!
    let video: Bool
    let voteAverage: Double
    let title: String!
    let popularity: Double
    let posterPath: String?
    let originalLanguage: String
    let originalTitle: String
    let genreIDS: [Int]
    let backdropPath: String?
    let adult: Bool
    let overview: String
    let releaseDate: String
    
    enum CodingKeys: String, CodingKey {
        case voteCount = "vote_count"
        case id = "id"
        case video = "video"
        case voteAverage = "vote_average"
        case title = "title"
        case popularity = "popularity"
        case posterPath = "poster_path"
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case genreIDS = "genre_ids"
        case backdropPath = "backdrop_path"
        case adult = "adult"
        case overview = "overview"
        case releaseDate = "release_date"
    }
    
    init(voteCount: Int, id: Int, video: Bool, voteAverage: Double, title: String, popularity: Double, posterPath: String, originalLanguage: String, originalTitle: String, genreIDS: [Int], backdropPath: String, adult: Bool, overview: String, releaseDate: String) {
        self.voteCount = voteCount
        self.id = id
        self.video = video
        self.voteAverage = voteAverage
        self.title = title
        self.popularity = popularity
        self.posterPath = posterPath
        self.originalLanguage = originalLanguage
        self.originalTitle = originalTitle
        self.genreIDS = genreIDS
        self.backdropPath = backdropPath
        self.adult = adult
        self.overview = overview
        self.releaseDate = releaseDate
    }
}

extension Movie: CatalogItemModelProtocol {
    var backgroundImage: Image? {
        guard let imagePath = self.backdropPath else {
            return nil
        }
        
        return Image(
            low: MoviesConfig.imagesBaseUrl + MoviesConfig.BackdropSizes.low.rawValue + imagePath,
            medium: MoviesConfig.imagesBaseUrl + MoviesConfig.BackdropSizes.medium.rawValue + imagePath,
            hight: MoviesConfig.imagesBaseUrl + MoviesConfig.BackdropSizes.hight.rawValue + imagePath)
    }
}
