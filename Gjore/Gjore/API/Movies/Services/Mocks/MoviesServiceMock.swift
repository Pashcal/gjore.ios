//
//  MoviesServiceMock.swift
//  Gjore
//
//  Created by Pavel Chupryna on 09/06/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation

enum MoviesServiceMockState {
    case none
    case data
}

class MoviesServiceMock: DetailsServiceProtocol {
    
    var state: MoviesServiceMockState
    var stateData: [MoviesServiceMockState: CardModelProtocol]!
    
    init(state: MoviesServiceMockState = .none) {
        self.state = state
        stateData = [:]
    }
    
    func getDetails(for id: Int, complete: @escaping (CardModelProtocol?) -> Void) {
        switch state {
        case .none:
            stateData[.none] = nil
            complete(nil)
        case .data:
            let mockData = MovieDetails(
                adult: false,
                backdropPath: "/mock_backdrop_\(id).jpg",
                belongsToCollection: nil,
                budget: 42,
                genres: [],
                homepage: nil,
                id: id,
                imdbID: nil,
                originalLanguage: "en",
                originalTitle: "Mock movie \(id)",
                overview: "Mock movie \(id) description",
                popularity: 5.0,
                posterPath: "/mock_poster_\(id).jpg",
                productionCompanies: [],
                productionCountries: [],
                releaseDate: "now",
                revenue: 100500,
                runtime: nil,
                spokenLanguages: [],
                status: "Mock status",
                tagline: nil,
                title: "Mock movie \(id)",
                video: false,
                voteAverage: 5.0,
                voteCount: 42)
            
            stateData[.data] = mockData
            complete(mockData)
        }
    }
}
