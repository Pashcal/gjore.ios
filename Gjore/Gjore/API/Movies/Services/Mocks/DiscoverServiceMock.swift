//
//  DiscoverServiceMock.swift
//  Gjore
//
//  Created by Pavel Chupryna on 09/06/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation

enum DiscoverServiceMockState {
    case none
    case empty
    case oneItem
    case fewItems
}

class DiscoverServiceMock: DiscoverServiceProtocol {
    
    var state: DiscoverServiceMockState
    var stateData: [DiscoverServiceMockState: [CatalogItemModelProtocol]]!
    
    init(state: DiscoverServiceMockState = .none) {
        self.state = state
        stateData = [:]
    }
    
    func getCatalogItems(complete: @escaping ([CatalogItemModelProtocol]?) -> Void) {
        switch state {
        case .none:
            stateData[.none] = nil
            complete(nil)
        case .empty:
            stateData[.empty] = []
            complete([])
        case .oneItem:
            let mockItem = [Movie(
                voteCount: 42,
                id: 1,
                video: false,
                voteAverage: 5.0,
                title: "Mock movie",
                popularity: 5.0,
                posterPath: "/mock_poster.jpg",
                originalLanguage: "en",
                originalTitle: "Mock movie",
                genreIDS: [],
                backdropPath: "/mock_backdrop.jpg",
                adult: false,
                overview: "Mock movie description",
                releaseDate: "now")]
            
            stateData[.oneItem] = mockItem
            complete(mockItem)
        case .fewItems:
            var mockItems: [Movie] = []
            for i in 1...10 {
                mockItems.append(Movie(
                    voteCount: 42,
                    id: i,
                    video: false,
                    voteAverage: 5.0,
                    title: "Mock movie \(i)",
                    popularity: 5.0,
                    posterPath: "/mock_poster_\(i).jpg",
                    originalLanguage: "en",
                    originalTitle: "Mock movie \(i)",
                    genreIDS: [],
                    backdropPath: "/mock_backdrop_\(i).jpg",
                    adult: false,
                    overview: "Mock movie \(i) description",
                    releaseDate: "now"))
            }
            
            stateData[.fewItems] = mockItems
            complete(mockItems)
        }
    }
}
