//
//  DiscoverService.swift
//  Gjore
//
//  Created by Pavel Chupryna on 10/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation

class DiscoverService: BaseService, DiscoverServiceProtocol {
    
    let moviesEndpoint: String = "/discover/movie"
    let tvShowsEndpoint: String = "/discover/tv"
    
    func getCatalogItems(complete: @escaping ([CatalogItemModelProtocol]?) -> Void) {
        super.get(moviesEndpoint, complete: { (data: PagedResults<Movie>?) in
            complete(data?.results)
        })
    }
}
