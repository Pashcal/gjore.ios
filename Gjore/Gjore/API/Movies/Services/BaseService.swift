//
//  BaseService.swift
//  Gjore
//
//  Created by Pavel Chupryna on 28/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation

class BaseService {
    
    let urlSessionConfig: URLSessionConfiguration
    
    init() {
        urlSessionConfig = URLSessionConfiguration.default
        urlSessionConfig.allowsCellularAccess = true
        urlSessionConfig.multipathServiceType = .handover
    }
    
    func get<T: Decodable>(_ endpoint: String, complete: @escaping (T?) -> Void, queryItems: [URLQueryItem] = []) {
        
        DispatchQueue(label: "getRequest").async {
            
            var urlComponents = URLComponents(string: MoviesConfig.baseUrl + endpoint)!
            let apiKeyQueryItem = URLQueryItem(name: "api_key", value: MoviesConfig.apiKey)
            urlComponents.queryItems = [apiKeyQueryItem] + queryItems
            
            let url = urlComponents.url!
            var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: ApiConfig.timout)
            urlRequest.httpMethod = "GET"
            urlRequest.addValue("application/json", forHTTPHeaderField: "content-type")
            
            let urlSession = URLSession(configuration: .default)
            print("DataTask reguest: \(String(describing: urlRequest.url))")
            let task = urlSession.dataTask(with: urlRequest) { data, response, error in
                let stringData = String(data: data!, encoding: String.Encoding.utf8)
                print("DataTask response: \(String(describing: response))\n" +
                      "with data: \(String(describing: stringData))")
                var result: T? = nil
                
                if let error = error {
                    print("DataTask error: \(error.localizedDescription)")
                }
                else if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                    let decoder = JSONDecoder()
                    do {
                        result = try decoder.decode(T.self, from: data)
                    }
                    catch let decodeError as NSError {
                        print("Decoding error: \(decodeError.localizedDescription)")
                        result = nil
                    }
                }
                
                DispatchQueue.main.async {
                    complete(result)
                }
            }
            task.resume()
            
        }
    }
}
