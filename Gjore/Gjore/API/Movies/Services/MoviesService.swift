//
//  MoviesService.swift
//  Gjore
//
//  Created by Pavel Chupryna on 28/05/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation

class MoviesService: BaseService, DetailsServiceProtocol {
    
    let detailsEndpoint: String = "/movie/"
    
    func getDetails(for id: Int, complete: @escaping (CardModelProtocol?) -> Void) {
        super.get(detailsEndpoint + String(id), complete: { (data: MovieDetails?) in
            complete(data)
        })
    }
}
