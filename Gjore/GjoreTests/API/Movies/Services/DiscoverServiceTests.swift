//
//  DiscoverServiceTests.swift
//  GjoreTests
//
//  Created by Pavel Chupryna on 08/06/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import XCTest
@testable import Gjore

class DiscoverServiceTests: XCTestCase {
    
    var discoverService: DiscoverService!
    
    override func setUp() {
        super.setUp()
        discoverService = DiscoverService()
    }
    
    override func tearDown() {
        discoverService = nil
        super.tearDown()
    }
    
    func testReachability() {
        
        let promise = expectation(description: "Respond object isn't nil")
        
        discoverService.getCatalogItems { (data: [CatalogItemModelProtocol]?) in
            promise.fulfill()
            XCTAssertNotNil(data)
        }
        
        waitForExpectations(timeout: ApiConfig.timout, handler: nil)
    }
}
