//
//  MoviesServiceTest.swift
//  GjoreTests
//
//  Created by Pavel Chupryna on 08/06/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import XCTest
@testable import Gjore

class MoviesServiceTests: XCTestCase {
    
    var moviesService: MoviesService!
    
    override func setUp() {
        super.setUp()
        moviesService = MoviesService()
    }
    
    override func tearDown() {
        moviesService = nil
        super.tearDown()
    }
    
    func testReachability() {
        
        let promise = expectation(description: "Respond object isn't nil")
        
        let movieId = 278
        moviesService.getDetails(for: movieId) { (data: CardModelProtocol?) in
            promise.fulfill()
            XCTAssertNotNil(data)
        }
        
        waitForExpectations(timeout: ApiConfig.timout, handler: nil)
    }
}
