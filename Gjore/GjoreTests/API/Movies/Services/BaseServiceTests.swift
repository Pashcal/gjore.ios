//
//  APIMoviesBaseServiceTest.swift
//  GjoreTests
//
//  Created by Pavel Chupryna on 07/06/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import XCTest
@testable import Gjore

class BaseServiceTests: XCTestCase {
    
    var baseService: BaseService!
    
    override func setUp() {
        super.setUp()
        baseService = BaseService()
    }
    
    override func tearDown() {
        baseService = nil
        super.tearDown()
    }
    
    func testConfiguration() {
        let config = baseService.urlSessionConfig
        
        XCTAssertTrue(config.allowsCellularAccess && config.multipathServiceType == .handover)
    }
    
    func testGetReachability() {
        
        let promise = expectation(description: "Respond object isn't nil")
        
        baseService.get("/configuration", complete: { (data: Configuration?) in
            promise.fulfill()
            XCTAssertNotNil(data)
        })
        
        waitForExpectations(timeout: ApiConfig.timout, handler: nil)
    }
}
