//
//  MoviesConfigTests.swift
//  GjoreTests
//
//  Created by Pavel Chupryna on 07/06/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import XCTest
@testable import Gjore

class MoviesConfigTests: XCTestCase {
    
    var testSession: URLSession!
    
    override func setUp() {
        super.setUp()
        testSession = URLSession(configuration: URLSessionConfiguration.default)
    }
    
    override func tearDown() {
        testSession = nil
        super.tearDown()
    }
    
    func testBaseUrl() {
        XCTAssertEqual(MoviesConfig.baseUrl, "https://api.themoviedb.org/3")
    }
    
    func testApiKeyExistence() {
        XCTAssertFalse(MoviesConfig.apiKey.isEmpty)
    }
    
    func testImagesConfig() {
        
        let promise = expectation(description: "Images config is correct")
        
        var configResult: Configuration? = nil
        
        let configUrl = URL(string: "\(MoviesConfig.baseUrl)/configuration?api_key=\(MoviesConfig.apiKey)")!
        let task = testSession.dataTask(with: configUrl) { data, response, error in
            if let error = error {
                XCTFail("DataTask error: \(error.localizedDescription)")
            }
            else if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                let decoder = JSONDecoder()
                do {
                    configResult = try decoder.decode(Configuration.self, from: data)
                    
                    promise.fulfill()
                }
                catch let decodeError as NSError {
                    XCTFail("Decoding error: \(decodeError.localizedDescription)")
                }
            }
        }
        task.resume()
        
        waitForExpectations(timeout: ApiConfig.timout, handler: nil)
        
        guard let config = configResult else {
            XCTAssertNotNil(configResult)
            return
        }
        
        XCTAssertEqual(MoviesConfig.imagesBaseUrl, config.images.secureBaseURL)
        XCTAssertTrue(config.images.backdropSizes.contains(MoviesConfig.BackdropSizes.low.rawValue), "Low image config is \"\(MoviesConfig.BackdropSizes.low.rawValue)\", but possible image configs is \(config.images.backdropSizes)")
        XCTAssertTrue(config.images.backdropSizes.contains(MoviesConfig.BackdropSizes.medium.rawValue), "Low image config is \"\(MoviesConfig.BackdropSizes.medium.rawValue)\", but possible image configs is \(config.images.backdropSizes)")
        XCTAssertTrue(config.images.backdropSizes.contains(MoviesConfig.BackdropSizes.hight.rawValue), "Low image config is \"\(MoviesConfig.BackdropSizes.hight.rawValue)\", but possible image configs is \(config.images.backdropSizes)")
    }
}
