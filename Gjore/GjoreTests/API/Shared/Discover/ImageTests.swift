//
//  ImageTests.swift
//  GjoreTests
//
//  Created by Pavel Chupryna on 08/06/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import XCTest
@testable import Gjore

class ImageTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        
        super.tearDown()
    }
    
    func testInit() {
        let lowImage = "low"
        let mediumImage = "medium"
        let hightImage = "hight"
        
        let image = Image(low: lowImage, medium: mediumImage, hight: hightImage)
        
        XCTAssertEqual(image.low, lowImage)
        XCTAssertEqual(image.medium, mediumImage)
        XCTAssertEqual(image.hight, hightImage)
    }
}
