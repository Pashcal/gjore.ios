//
//  CatalogViewModelTests.swift
//  GjoreTests
//
//  Created by Pavel Chupryna on 09/06/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import XCTest
@testable import Gjore

class CatalogViewModelTests: XCTestCase {
    
    var discoverService: DiscoverServiceMock!
    
    override func setUp() {
        super.setUp()
        discoverService = DiscoverServiceMock(state: .none)
    }
    
    override func tearDown() {
        discoverService = nil
        super.tearDown()
    }
    
    func testInit() {
        let vm = CatalogViewModel(nil)
        
        XCTAssertNil(vm.items)
    }
    
    func testLoadCatalogNone() {
        discoverService.state = .none
        let vm = CatalogViewModel(nil, discoverService: discoverService)
        
        vm.onViewDidLoad()
        
        XCTAssertNil(vm.items)
    }
    
    func testLoadCatalogEmpty() {
        discoverService.state = .empty
        let vm = CatalogViewModel(nil, discoverService: discoverService)
        
        vm.onViewDidLoad()
        
        guard let items = vm.items else {
            XCTAssertNotNil(vm.items)
            return
        }
        
        XCTAssertTrue(items.isEmpty)
    }
    
    func testLoadCatalogOneItem() {
        discoverService.state = .oneItem
        let vm = CatalogViewModel(nil, discoverService: discoverService)
        
        vm.onViewDidLoad()
        let mockItems = discoverService.stateData[.oneItem]
        
        guard let items = vm.items else {
            XCTAssertNotNil(vm.items)
            return
        }
        
        XCTAssertTrue(items.count == 1)
        XCTAssertEqual(items.first, CatalogItemViewModel(mockItems!.first!))
    }
    
    func testLoadCatalogFewItem() {
        discoverService.state = .fewItems
        let vm = CatalogViewModel(nil, discoverService: discoverService)
        
        vm.onViewDidLoad()
        let mockItems = discoverService.stateData[.fewItems]
        
        guard let items = vm.items else {
            XCTAssertNotNil(vm.items)
            return
        }
        
        XCTAssertTrue(items.count == mockItems?.count)
        for i in 0..<items.count {
            XCTAssertEqual(items[i], CatalogItemViewModel(mockItems![i]), "Elements on position \"\(i)\" are not equal")
        }
    }
}
