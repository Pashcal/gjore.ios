//
//  CatalogItemViewModelTests.swift
//  GjoreTests
//
//  Created by Pavel Chupryna on 09/06/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import XCTest
@testable import Gjore

class CatalogItemViewModelTests: XCTestCase {
    
    var item: Movie!
    
    override func setUp() {
        super.setUp()
        item = Movie(
            voteCount: 42,
            id: 1,
            video: false,
            voteAverage: 5.0,
            title: "Mock movie",
            popularity: 5.0,
            posterPath: "/mock_poster.jpg",
            originalLanguage: "en",
            originalTitle: "Mock movie",
            genreIDS: [],
            backdropPath: "/mock_backdrop.jpg",
            adult: false,
            overview: "Mock movie description",
            releaseDate: "now")
    }
    
    override func tearDown() {
        item = nil
        super.tearDown()
    }
    
    func testInit() {
        let vm = CatalogItemViewModel(item)
        
        XCTAssertEqual(vm.id, item.id)
        XCTAssertEqual(vm.backgroundImage, item.backgroundImage)
        XCTAssertEqual(vm.title, item.title)
    }
}
