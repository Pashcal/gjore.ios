//
//  CardViewModelTests.swift
//  GjoreTests
//
//  Created by Pavel Chupryna on 09/06/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import XCTest
@testable import Gjore

class CardViewModelTests: XCTestCase {
    
    var itemVM: CatalogItemViewModel!
    var detailsService: MoviesServiceMock!
    var discoverRouter: DiscoverRouter!
    
    override func setUp() {
        super.setUp()
        itemVM = CatalogItemViewModel(Movie(
            voteCount: 42,
            id: 1,
            video: false,
            voteAverage: 5.0,
            title: "Mock movie",
            popularity: 5.0,
            posterPath: "/mock_poster.jpg",
            originalLanguage: "en",
            originalTitle: "Mock movie",
            genreIDS: [],
            backdropPath: "/mock_backdrop.jpg",
            adult: false,
            overview: "Mock movie description",
            releaseDate: "now"))
        
        detailsService = MoviesServiceMock(state: .none)
    }
    
    override func tearDown() {
        itemVM = nil
        detailsService = nil
        super.tearDown()
    }
    
    func testInit() {
        let vm = CardViewModel(nil, catalogItem: itemVM)
        
        XCTAssertEqual(vm.id, itemVM.id)
        XCTAssertEqual(vm.headerImage, itemVM.backgroundImage)
        XCTAssertEqual(vm.title, itemVM.title)
    }
    
    func testLoadDetailsNone() {
        detailsService.state = .none
        let vm = CardViewModel(nil, catalogItem: itemVM, detailsService: detailsService)
        
        vm.onViewDidLoad()
        
        guard let description = vm.description else {
            XCTAssertNotNil(vm.description)
            return
        }
        
        XCTAssertTrue(description.isEmpty)
    }
    
    func testLoadDetails() {
        detailsService.state = .data
        let vm = CardViewModel(nil, catalogItem: itemVM, detailsService: detailsService)
        
        vm.onViewDidLoad()
        let mockData = detailsService.stateData[.data]
        
        guard let description = vm.description else {
            XCTAssertNotNil(vm.description)
            return
        }
        
        XCTAssertEqual(description, mockData?.description)
    }
}
